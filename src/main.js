const express = require("express");
const makeStoppable = require("stoppable")
const http = require("http");
const path = require('path');

const cookeiSession= require('cookie-session');

const bodyParser = require('body-parser');

const TasksService = require('./services/TasksService');
const tasksService = new TasksService();

const routes = require('./routes')
const app = express();

app.use(
  cookeiSession({
    name:'session',
    keys:['sdfax123']
  })
)

app.use(bodyParser.urlencoded({exteded:true}));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));

app.use(express.static(path.join(__dirname,'../assets')))

app.use(async(req,res, next)=>{
   try{
      const tasks= await tasksService.getTasks();
      res.locals.dones=tasks;
      console.log(res.locals.dones);
      return next();
   }catch(err){
    return next(err)
   }
})

app.use(
  '/',
  routes({
  tasksService,
}))
const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
