const express = require('express');

const router = express.Router();

module.exports=(params)=>{
   const {tasksService} =params;
    router.get('/',async (req,res)=>{
      const dones=await tasksService.getDones();
      res.render('layout',{pageTitle:'Hello again', template:'done',dones})
       // return res.send(tasks)
    })
    return router;
}