const express = require('express');
const doneRoutes = require('./done')
const router = express.Router();
const {check, validationResult} = require('express-validator');

module.exports=(params)=>{
    const {tasksService} =params;

    router.get('/',async (req,res,next)=>{
      try{
        //const errors = req.session.feedback ? req.session.feedback.error :false;
      const error = req.session.feedback ? req.session.feedback.error :-1;
        req.session.feedback={};
        const todos= await tasksService.getTasks(); 
        res.render('layout',{pageTitle:'Hello again', template:'index',todos,error})
      }catch(err){
           return next(err)
      }  
    })

    router.post('/',[
        check('task')
        .trim()
        .isLength({min:3})
        .escape()
        .withMessage('Length of the task must be 4 characters or higher !')
    ], async (req,res)=>{
        console.log(req.body);
        const error = validationResult(req);
        if(!error.isEmpty()){
            req.session.feedback= {error:1};
          return res.redirect('/');
        }
        else{
            await tasksService.addTask(req.body.task);
            req.session.feedback= {error:0};
          return res.redirect('/');
        }
    })

 router.post('/api/tasks/:taskName/done', async(req, res) => {
        const taskName = req.params.taskName;
      
        const tasks = await tasksService.getTasks();
        const task = tasks.find(task => task === taskName);
      
        if (!task) {
          return res.status(404).json({ error: 'Task not found' });
        }
      
        await tasksService.getDone(task);
      
        // Return a success response
        res.json({ message: 'Task marked as done' });
      });

    router.use('/done', doneRoutes(params));
    return router;
}