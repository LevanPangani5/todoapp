class TasksService {
    constructor() {
      this.todoTasks = ['this is task'];
      this.doneTasks = ['this is done task'];
    }
  
    async loadTasks() {
      return new Promise((resolve, reject) => {
        
        resolve({
          todo: this.todoTasks,
          done: this.doneTasks,
        });
      });
    }
  
    async saveTasks() {
      return new Promise((resolve, reject) => {
        resolve();
      });
    }
  
    async getDone(todo) {
      return new Promise(async (resolve, reject) => {
        const todoIndex = this.todoTasks.indexOf(todo);
        if (todoIndex===-1) {
          reject(new Error('Invalid todo index.'));
        }

        const doneTask = this.todoTasks.splice(todoIndex, 1)[0];
        this.doneTasks.unshift(doneTask);
  
        try {
          await this.saveTasks();
          resolve();
        } catch (error) {
          reject(error);
        }
      });
    }
  
    async addTask(task) {
      return new Promise(async (resolve, reject) => {
        this.todoTasks.push(task);
  
        try {
          await this.saveTasks();
          resolve();
        } catch (error) {
          reject(error);
        }
      });
    }

    async getTasks() {

        return new Promise(async (resolve, reject) => {
          try {
            resolve(this.todoTasks);
          } catch (error) {
            reject(error);
          }
        });
      }

      async getDones() {

        return new Promise(async (resolve, reject) => {
          try {
            resolve(this.doneTasks);
          } catch (error) {
            reject(error);
          }
        });
      }
  }
  
  module.exports = TasksService;
  